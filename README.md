# Puzzle Bobble

Game can be played at : https://miscavel.gitlab.io/html5/game/shopee/puzzle-bobble

NB :
1. "webpack --watch & live-server --port=8085" may prevent web-server from launching (as the cmd is stuck at webpack --watch)

Solution: run 2 separate cmd windows, run 'node_modules\\.bin\webpack --watch' on the first one, run 'node_modules\\.bin\live-server --port=8085' on the second window

OR

run 'start node_modules\\.bin\webpack --watch', then run 'start node_modules\\.bin\live-server --port=8085'

2. Run 'npm run build' to get build.min.js in /dist which can be deployed on production