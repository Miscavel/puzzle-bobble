//Config for each spritesheet used

export const BubbleSpriteConfig = {
    name: 'bubble',
    url: 'src/Assets/bubblesprite.png',
    frame: {
        frameWidth: 180,
        frameHeight: 180
    }
};

export const ArrowSpriteConfig = {
    name: 'arrow',
    url: 'src/Assets/arrow.png',
    frame: {
        frameWidth: 295,
        frameHeight: 295
    }
}

export const GameOverPanelSpriteConfig = {
    name: 'gameOverPanel',
    url: 'src/Assets/Panel.png',
    frame: {
        frameWidth: 500,
        frameHeight: 362
    }
}

export const RestartSpriteConfig = {
    name: 'restart',
    url: 'src/Assets/Replay.png',
    frame: {
        frameWidth: 230,
        frameHeight: 218
    }
}