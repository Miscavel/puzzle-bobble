import "phaser";

export const DEFAULT_WIDTH: number = 760;
export const DEFAULT_HEIGHT: number = 1280;

const config: Phaser.Types.Core.GameConfig = {
  title: "Puzzle Bobble",
  scale: {
    parent: "game",
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: DEFAULT_WIDTH,
    height: DEFAULT_HEIGHT
  },
  physics: {
    default: "arcade",
    arcade: {
      debug: false
    }
  },
  backgroundColor: "#6A4775"
};

export default config;