import "phaser";
import { DEFAULT_WIDTH, DEFAULT_HEIGHT } from './Config';
import UIConfig from './UIConfig';
import BubbleGroupConfig from './BubbleGroupConfig';

export default {
    ball: {
        //Position of shooter
        position: new Phaser.Math.Vector2(DEFAULT_WIDTH / 2, UIConfig.segment.shooter.y)
    },
    arrow: {
        line: {
            width: 8,
            color: 0xff0000
        },
        tip: {
            color: 0xff0000
        },
        cursor: {
            width: 9,
            color: 0xff0000,
            count: 360,
            length: DEFAULT_HEIGHT * 6,
            gap: DEFAULT_HEIGHT * 6 / 360,
            inactivePosition: new Phaser.Math.Vector2(-9999, -9999)
        }
    },
    //Limit of angle between pointer and shooter to allow shooting
    angleLimit: 82.5, 
    //Limit cursor position, below which shooter will be deactivated
    deactivateBoundary: new Phaser.Math.Vector2(0, UIConfig.segment.shooter.y),
    //Speed at which the ball is launched in the cursor's direction
    shootSpeed: 2000,
    deathBoundary: Math.floor(BubbleGroupConfig.startingPosition.y + (BubbleGroupConfig.totalRow - 1) * BubbleGroupConfig.increment.y)
}