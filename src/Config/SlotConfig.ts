import "phaser";
import BubbleGroupConfig from './BubbleGroupConfig';

//Config for the slot object
export default {
    /*
        neighborOffsets is used to determine the position of a slot's neighbors.
        It would contain 6 sets of offsets, one for each direction:
    */
    neighborOffsets: [
        //1. Upper Left
        new Phaser.Math.Vector2(BubbleGroupConfig.increment.x * -0.5, BubbleGroupConfig.increment.y * -1),
        //2. Upper Right
        new Phaser.Math.Vector2(BubbleGroupConfig.increment.x * 0.5, BubbleGroupConfig.increment.y * -1),
        //3. Left
        new Phaser.Math.Vector2(BubbleGroupConfig.increment.x * -1, 0),
        //4. Right
        new Phaser.Math.Vector2(BubbleGroupConfig.increment.x, 0),
        //5. Lower Left
        new Phaser.Math.Vector2(BubbleGroupConfig.increment.x * -0.5, BubbleGroupConfig.increment.y),
        //6. Lower Right
        new Phaser.Math.Vector2(BubbleGroupConfig.increment.x * 0.5, BubbleGroupConfig.increment.y)
    ]
}