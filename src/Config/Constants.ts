import "phaser";
//File that contains constants that may be reused throughout the game

export default {
    DEG_TO_RAD: 3.14 / 180,
    vector: {
        down: new Phaser.Math.Vector2(0, 1)
    }
}