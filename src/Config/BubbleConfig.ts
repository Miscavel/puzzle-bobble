import "phaser";
import { DEFAULT_WIDTH, DEFAULT_HEIGHT } from './Config';
import { BubbleSpriteConfig } from './SpriteConfig';

//Config for bubble object
export default {
    gameObject: {
        /*
            Length of sides of the gameObject itself.
            Adjusted to ensure that 8 bubbles would perfectly fit a row
        */
        diameter: DEFAULT_WIDTH * 3 / 16 
    },
    bubble: {
        /*
            Radius of the displayed bubble sprite.
            Diameter of the bubble is set to 2/3 of the gameObject's diameter 
            since 1/3 is taken by blank space.
        */
        radius: DEFAULT_WIDTH / 16, 
        diameter: DEFAULT_WIDTH / 8
    },
    hitBox: {
        /*
            Radius of the bubble's hitbox.
            Adjusted to ensure that the hitbox perfectly clamps the bubble sprite.
        */
        radius: DEFAULT_WIDTH * 1.25 / 16, 
        offset: { 
            //Offset to centralize hitbox in bubble's sprite
            x: (BubbleSpriteConfig.frame.frameWidth / 2) - (DEFAULT_WIDTH * 1.25 / 16),
            y: (BubbleSpriteConfig.frame.frameHeight / 2) - (DEFAULT_WIDTH * 1.25 / 16)
        }
    },
    colors: [
        //All the possible colors for a bubble
        0xff0000, //Red
        0x00ff00, //Green
        0x0000ff, //Blue
        0xffffff, //White
        0xff66cc, //Pink
        0x800080, //Purple
        0xffaa1d, //Orange
    ],
    bedColor: 0x000000, //Black, color for unbreakable bubbles
    popCount: 3, //Number of bubbles of the same color required to trigger popping
    inactivePosition: new Phaser.Math.Vector2(-9999, -9999), //Position to hide inactive bubbles
    popDelay: 100, //Delay in ms between each bubble pop
    fallGravity: new Phaser.Math.Vector2(0, 400), //Acceleration for falling bubbles
    fallDepth: 1, //Depth (z-index) of bubble when falling
    fallBoundary: new Phaser.Math.Vector2(0, DEFAULT_HEIGHT * 0.7) //Bubble disappears upon touching boundary
}