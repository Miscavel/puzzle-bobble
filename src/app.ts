import "phaser";
import Config from './Config/Config';
import { PreloadScene } from './Scripts/Scene/PreloadScene';
import { GameScene } from './Scripts/Scene/GameScene';
import { PreloadSceneKey, GameSceneKey } from './Config/SceneConfig';

export class PuzzleBobble extends Phaser.Game {
    constructor(config: Phaser.Types.Core.GameConfig) {
        super(config);
        this.scene.add(PreloadSceneKey, PreloadScene);
        this.scene.add(GameSceneKey, GameScene);
        this.scene.start(PreloadSceneKey);
    }
}

window.onload = () => {
    var game = new PuzzleBobble(Config);
};