import "phaser";
import { PreloadSceneKey, GameSceneKey } from '../../Config/SceneConfig';
import { BubbleSpriteConfig, ArrowSpriteConfig, GameOverPanelSpriteConfig, RestartSpriteConfig } from '../../Config/SpriteConfig';
import { BubbleAnimationConfig } from '../../Config/AnimationConfig';
import AudioConfig from '../../Config/AudioConfig';

export class PreloadScene extends Phaser.Scene {
    constructor() {
        super({
            key: PreloadSceneKey
        });
    }

    loadSpritesheets(): void {
        //Load spritesheets
        this.load.spritesheet(BubbleSpriteConfig.name, BubbleSpriteConfig.url, BubbleSpriteConfig.frame);
    }

    loadImages(): void {
        //Load images
        this.load.image(ArrowSpriteConfig.name, ArrowSpriteConfig.url);
        this.load.image(GameOverPanelSpriteConfig.name, GameOverPanelSpriteConfig.url);
        this.load.image(RestartSpriteConfig.name, RestartSpriteConfig.url);
    }

    generateAnimations(): void {
        //Generate animations
        BubbleAnimationConfig.forEach(function (value) {
            this.anims.create({
                key: value.key,
                frames: this.anims.generateFrameNumbers(BubbleSpriteConfig.name, {frames: value.frames}),
                frameRate: value.frameRate,
                repeat: value.repeat
            });
        }.bind(this));
    }
    
    loadAudio(): void {
        //Load sound effects used in game
        this.load.audio(AudioConfig.pop.name, AudioConfig.pop.url);
    }

    preload(): void {
        this.loadSpritesheets();
        this.loadImages();
        this.loadAudio();

        //Once all assets are loaded, go to game scene
        this.load.on('complete', function () {
            this.generateAnimations();
            this.scene.start(GameSceneKey);
        }.bind(this));
    }

    create(): void {
        
    }
}