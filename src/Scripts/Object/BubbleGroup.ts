import "phaser";
import BubbleGroupConfig from '../../Config/BubbleGroupConfig';
import Slot from './Slot';
import Bubble from "./Bubble";
import SlotConfig from '../../Config/SlotConfig';
import ScoreManager from "../Manager/ScoreManager";
import { GameScene } from '../Scene/GameScene';
import BubbleConfig from "../../Config/BubbleConfig";
import DepthConfig from "../../Config/DepthConfig";

export default class BubbleGroup
{
    /*
        'slots': hashmap to contain all the available slots in the game.
        'bubblePool': pool of bubbles that can be recycled.
        'scene': scene to which this BubbleGroup belongs
        'popCount': a number that keeps track whether the current popping sequence has finished.
            1. popCount is set to the number of bubbles in a pop chain when triggered
            2. popCount is reduced by 1 everytime a bubble completes its popping animation
            3. Once popCount reaches 0, then the said popping sequence has finished
            4. When there is an ongoing popping sequence (popCount > 0), prevent 
            any new bubble interactions.       
        'score': the object responsible for handling the score (display, add, reset) 
        'bubbleCollection': collection of bubbles that can collide with the shooter's bubble
    */
    slots: {};
    bubblePool: Array<Bubble>;
    scene: GameScene;
    popCount: number;
    score: ScoreManager;
    bubbleCollection: Phaser.Physics.Arcade.Group;

    constructor(scene: GameScene) {
        this.slots = {};
        this.bubblePool = [];
        this.scene = scene;
        this.popCount = 0; //Initially, popCount is 0 and bubble interaction is allowed
        this.score = ScoreManager.getInstance();
        this.bubbleCollection = this.scene.physics.add.group();

        this.generateSlots();
        this.discoverNeighbors();
    }

    /*
        Generate the slots available in the game.
        Slots refer to spaces that can be filled by bubbles.
        Since the game can contain a maximum of 10 rows of bubbles, there are 10 rows 
        of slots available.
        In the beginning of the game, the first 5 rows of slots will be filled with bubbles
        whereas the rest will have 'undefined' set in their bubble variable.
        
        Slots are stored in a hashmap for easy reference call, for example to get the slot at
        position {47.5, 47.5}, simply call this.slots["47|47"] (rounded down to improve accuracy).
        This functionality will be used in assigning slot neighbors down the line, which are 
        consequently used to determine chain poppings when 3 or more bubbles of the same color match.
    */
    generateSlots(): void {
        let y = BubbleGroupConfig.startingPosition.y; 
        for (let i = 0; i < BubbleGroupConfig.totalRow; i++)
        {
            //Determines if row is an even [0] or an odd [1]
            let evenOdd = i % 2; 

            //X-coordinate for the first bubble in the row
            let x = BubbleGroupConfig.startingPosition.x[evenOdd]; 

            //Number of bubbles to be generated in the row
            let count = BubbleGroupConfig.columnCount[evenOdd]; 
            for (let j = 0; j < count; j++)
            {
                //Construct new vector2 for position memorization
                const position = new Phaser.Math.Vector2(x, y);

                //Instantiate bubble only if i is below the initialRow to be generated
                let bubble: Bubble = undefined;
                if (i < BubbleGroupConfig.initialRow)
                {
                    bubble = new Bubble(this, position);
                }

                //Initial rows that cannot be popped (hidden)
                if (i < BubbleGroupConfig.bedRow)
                {  
                    bubble.setColor(BubbleConfig.bedColor);
                    bubble.sprite.setDepth(DepthConfig.bed);
                    bubble.sprite.setVisible(false);
                }

                /*
                    Construct a key with format x|y for the slot hashmap index.
                    Coordinates are floored to improve accuracy.
                    For example, if unfloored, coordinate like '171.32' might be missed if we 
                    happen to hit '171.31' due to calculation rounding.
                */
                const key = Math.floor(position.x) + "|" + Math.floor(position.y);
                
                //Instantiate a new slot object for that hashmap index
                this.slots[key] = new Slot(position, bubble, this, key);

                //Increment x for the next bubble in the row
                x += BubbleGroupConfig.increment.x;
            }
            //Increment y for the next row
            y += BubbleGroupConfig.increment.y;
        }
    }

    discoverNeighbors(): void {
        Object.keys(this.slots).forEach(function (key: string) {
            /*
                Check the 6 positions surrounding the slot. (Refer to SlotConfig file)
                If a slot exists in the given position, adds it as the inspected slot's neighbor.
            */
            SlotConfig.neighborOffsets.forEach(function (offset: Phaser.Math.Vector2) {
                //Coordinates are floored since hashmap stores floored cordinates
                const slot: Slot = this.slots[key];
                const x = Math.floor(slot.position.x - offset.x);
                const y = Math.floor(slot.position.y - offset.y);

                //Construct the hashKey in x|y format
                const hashKey = x + "|" + y;
                
                //If hashKey exists in hashmap, registers the related slot to the inspected slot
                const neighbor: Slot = this.slots[hashKey];
                if (neighbor)
                {
                    slot.registerNeighbor(neighbor);
                }
            }.bind(this));
        }.bind(this));
    }

    //Bubble falling purposes
    traceAnchoredBubbles(): void {
        /*
            Function to check which bubbles are not anchored to the top of the game board.
            These bubbles would then fall out to the bottom of the board.
        */
        let row: number = 0; //First row
        let x: number = BubbleGroupConfig.startingPosition.x[row]; //First slot's x-coordinate
        let y: number = BubbleGroupConfig.startingPosition.y; //First slot's y-coordinate

        //Loop through the first row of slots (anchored)
        for (let i = 0; i < BubbleGroupConfig.columnCount[row]; i++)
        {
            //Generate the hashmap key
            const key: string = Math.floor(x) + "|" + Math.floor(y);
            
            //Get the associated slot
            const slot: Slot = this.slots[key];

            //If the slot has bubble
            if (slot.bubble)
            {
                //Recursively trace which bubbles are connected to it, and set them as 'anchored'
                this.recursivelyAnchorBubbles(slot);
            }

            //Go to the next slot
            x += BubbleGroupConfig.increment.x;
        }
        this.releaseNonAnchoredBubbles();
    }
    recursivelyAnchorBubbles(slot: Slot): void {
        //If slot does not have bubble, skip
        if (!slot.bubble)
        {
            return;
        }

        //If slot has been anchored, skip
        if (slot.anchored)
        {
            return;
        }

        //Set slot as anchored as it is rooted to the top row
        slot.anchored = true;

        //Recursively repeat the process for each neighbor
        slot.neighbors.forEach(function (neighbor: Slot) {
            this.recursivelyAnchorBubbles(neighbor);
        }.bind(this));
    }
    releaseNonAnchoredBubbles(): void {
        /*
            Function to make bubbles that are not anchored fall down to the bottom of the board.
        */
        Object.keys(this.slots).forEach(function (key: string) {
            const slot: Slot = this.slots[key];

            //If slot has bubble
            if (slot.bubble)
            {
                //If bubble is not anchored to the top
                if (!slot.anchored)
                {
                    //Make the bubble fall down
                    slot.bubble.fall();
                }
            }

            //Reset anchor status for next popping sequence
            slot.anchored = false; 
        }.bind(this));
    }
}