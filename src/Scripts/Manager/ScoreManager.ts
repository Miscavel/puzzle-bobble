import "phaser";
import TextConfig from '../../Config/TextConfig';

export default class ScoreManager {
    /*
        ScoreManager class to manage score increments, additions, and update score text
    */
    private static instance: ScoreManager;
    private score: number;
    private text: Phaser.GameObjects.Text;
    
    private constructor() {
        this.reset();
    }

    reset(): void {
        this.setScore(0);
    }

    setScore(pts: number): void {
        this.score = pts;
        this.updateText();
    }

    addScore(pts: number): void {
        this.setScore(this.score + pts);
    }

    getScore(): number {
        return this.score;
    }

    setText(object: Phaser.GameObjects.Text): void {
        this.text = object;
        this.reset();
    }

    updateText(): void {
        if (this.text)
        {
            this.text.setText('Score: ' + this.score);
            this.text.x = TextConfig.score.position.x - this.text.width / 2;
            this.text.y = TextConfig.score.position.y - this.text.height / 2;
        }
    }

    static getInstance(): ScoreManager {
        if (!ScoreManager.instance)
        {
            ScoreManager.instance = new ScoreManager();
        }
        return ScoreManager.instance;
    }
}